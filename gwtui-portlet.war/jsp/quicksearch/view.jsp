<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<portlet:defineObjects />

	<fmt:setBundle basename="com.ail.ui.client.common.i18n.Messages" var="lang"/>

	<table class="gui-cell-border" width="100%">
		<tr class="gui-cell-border">
			<td class="gui-title-text gui-standard-cell">
				<span id="gui-QUICKSEARCH-search-title"></span>
			</td>
		</tr>
		<tr>
			<td class="gui-standard-text gui-standard-cell"><fmt:message key="enterQuoteNumber" bundle="${lang}"/></td>
			<td class="gui-standard-text gui-standard-cell" style="position: absolute; left: 51%"><fmt:message key="enterDate" bundle="${lang}"/></td>
			<!-- gui-standard-text gui-standard-cell abs-left-align -->
		</tr>
		<tr>
		
		</tr>
		<tr>
			<td class="gui-standard-text gui-standard-cell">
			<table>
				<tr>
					<td><span id="gui-QUICKSEARCH-search-field-container"></span></td>
					<td><span id="gui-QUICKSEARCH-search-date-field-container"></span></td>
				
				</tr>
				
			
				
				<tr>
				    <td class="gui-standard-text gui-standard-cell"><fmt:message key="PolicyCheck" bundle="${lang}"/></td>
				    <td><span id="gui-QUICKSEARCH-search-policyStatus-field-container"></span></td>
				</tr>
				
				<tr>
					<td><span id="gui-QUICKSEARCH-search-button-container"></span></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td id="gui-QUICKSEARCH-error-label-container" class="gui-standard-text" align="center"></td>
		</tr>
	</table>
