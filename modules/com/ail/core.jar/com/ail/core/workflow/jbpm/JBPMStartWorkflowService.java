/* Copyright Applied Industrial Logic Limited 2003. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.core.workflow.jbpm;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.List;

import org.apache.commons.codec.CharEncoding;
import org.apache.commons.lang.StringUtils;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;

import com.ail.annotation.ServiceImplementation;
import com.ail.core.Attribute;
import com.ail.core.CoreProxy;
import com.ail.core.PostconditionException;
import com.ail.core.PreconditionException;
import com.ail.core.Service;
import com.ail.core.workflow.StartWorkflowService.StartWorkflowArgument;
import com.ail.core.workflow.WorkflowHelper;
/**
 * Service implementation to start a workflow. Invokes a rest service in jBPM and checks only for a successful 200
 * response, does not read any other data.
 */
@ServiceImplementation
public class JBPMStartWorkflowService extends Service<StartWorkflowArgument> {

    @Override
    public void invoke() throws PreconditionException {
        if (WorkflowHelper.isExecuteOnChangeField(args.getModelArg(), args.getOnChangeFieldArg())) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        ClientRequest restRequest = null;
                        JBPMRestHelper restHelper = new JBPMRestHelper(args.getPropertiesArg());
                        try {
                            restRequest = restHelper.getProcessRequest(getPath());
                        } catch (URISyntaxException | UnsupportedEncodingException e) {
                            throw new PreconditionException("Could not create start workflow request.", e);
                        }

                        ClientResponse<String> response = restRequest.post(String.class);
                        if (response.getStatus() != 200) {
                            throw new PostconditionException(response.getStatus() + " response for start workflow.");
                        }
                    } catch (Exception e) {
                        new CoreProxy().logError("StartWorkflowCommand failed.", e);
                    }
                }
            });
            t.start();
        }
    }

    /**
     * Get the path to the rest call to start a workflow. May pass a caseId and/or a productId in the workflow map parameter.
     * @return
     * @throws UnsupportedEncodingException
     */
    private String getPath() throws UnsupportedEncodingException {
        String path = args.getWorkflowIdArg() + "/start";

        String pathArgs = "" ;

        if (args.getCaseTypeArg() != null) {
            String caseId = args.getModelArg().getExternalSystemId();
            String typeId = args.getCaseTypeArg().getName().toLowerCase() + "Id";
            pathArgs += "map_" + typeId + "=" + URLEncoder.encode(caseId, CharEncoding.UTF_8);
        }

        if (StringUtils.isNotBlank(args.getProductIdArg())) {
            if (!pathArgs.isEmpty()) {
                pathArgs += "&";
            }
            pathArgs += "map_productId=" + URLEncoder.encode(args.getProductIdArg(), CharEncoding.UTF_8);
        }

        List<Attribute> attributes = args.getAttributeArg();
        if (attributes != null) {
            for (Attribute att : attributes) {
                // need to avoid duplicate parameters
                if (!pathArgs.contains("map_" + att.getId())) {
                    if (!pathArgs.isEmpty()) {
                        pathArgs += "&";
                    }
                    pathArgs += "map_" + att.getId() + "=" + URLEncoder.encode(att.getValue(), CharEncoding.UTF_8);
                }
            }
        }

        return path + (StringUtils.isNotBlank(pathArgs) ? "?" + pathArgs : "");
    }
}