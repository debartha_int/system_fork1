/* Copyright Applied Industrial Logic Limited 2003. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package com.ail.core.document;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;

import com.ail.annotation.ServiceImplementation;
import com.ail.core.CoreContext;
import com.ail.core.PostconditionException;
import com.ail.core.PreconditionException;
import com.ail.core.Service;
import com.ail.core.Type;
import com.ail.core.language.I18N;

import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.converter.XDocConverterException;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.core.document.SyntaxKind;
import fr.opensagres.xdocreport.core.io.XDocArchive;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.images.FileImageProvider;
import fr.opensagres.xdocreport.document.images.IImageProvider;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.FieldExtractor;
import fr.opensagres.xdocreport.template.FieldsExtractor;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata;



/**
 * This class provides an implementation of the render document service which renders to PDF
 * using iText.<p/>
 */
@ServiceImplementation
public class RenderXDocReportPdfDocumentService extends Service<RenderDocumentService.RenderDocumentArgument> {

    @Override
    public void invoke() throws PreconditionException, PostconditionException, RenderException {
        if (args.getSourceDataArg()==null) {
            throw new PreconditionException("args.getSourceDataArg()==null");
        }

        if (args.getTemplateUrlArg()==null || args.getTemplateUrlArg().length()==0) {
            throw new PreconditionException("args.getTemplateUrlArg()==null || args.getTemplateUrlArg().length()==0");
        }

        boolean pdfaConformance = false;
        if (args.getPDFaConformanceArg()!=null && args.getPDFaConformanceArg().equals("TRUE")) {
            pdfaConformance = true;
        }

        // The resulting PDF will end up in this array
        ByteArrayOutputStream output=new ByteArrayOutputStream();
        Type data = null;

        try {
            data=(Type)core.fromXML(args.getSourceDataArg().getType(), args.getSourceDataArg());
        } catch (Exception e) {
            throw new PreconditionException("Failed to translate source data into an object.", e);
        }


        String xpath=null;

        // Load DOCX file and set Velocity template engine and cache it to the registry
        InputStream inTemplate;
        try {
            inTemplate = (new URL(args.getTemplateUrlArg())).openStream();
        }
        catch (MalformedURLException e) {
            throw new RenderException("Template URL issue: '"+args.getTranslationUrlArg()+"' "+e);
        }
        catch (IOException e) {
            throw new RenderException("IO Exception during template read: '"+args.getTranslationUrlArg()+"' "+e);
        }

        IXDocReport report;
        try {
            report = XDocReportRegistry.getRegistry().loadReport(inTemplate,TemplateEngineKind.Velocity);
        } catch (IOException e) {
            throw new RenderException("IO Exception during template load: '"+args.getTranslationUrlArg()+"' "+e);
        } catch (XDocReportException e) {
            throw new RenderException("XDoc Report Exception during template read: '"+args.getTranslationUrlArg()+"' "+e);
        }

        FieldsMetadata metadata = report.createFieldsMetadata();

        // create copy for looping around field names without destroying field metadata
        report.setCacheOriginalDocument(true);
        XDocArchive arch = report.getOriginalDocumentArchive();
        IXDocReport reportCopy;
        try {
            reportCopy = XDocReportRegistry.getRegistry().createReport(arch.createCopy());
        } catch (IOException e) {
            throw new RenderException("IO Exception during create report copy: '"+args.getTranslationUrlArg()+"' "+e);
        } catch (XDocReportException e) {
            throw new RenderException("XDoc Report Exception during create report copy: '"+args.getTranslationUrlArg()+"' "+e);
        }
        reportCopy.setTemplateEngine(report.getTemplateEngine());

        // get field names in copy and set meta data (syntax) for fields in original
        HashMap<String,Boolean> fields = new HashMap<>();
        FieldsExtractor<FieldExtractor> copyFieldsExtractor = FieldsExtractor.create();
        try {
            reportCopy.extractFields(copyFieldsExtractor);
        } catch (XDocReportException e) {
            throw new RenderException("XDoc Report Exception during report copy field read: '"+args.getTranslationUrlArg()+"' "+e);
        } catch (IOException e) {
            throw new RenderException("IO Exception during report copy field read: '"+args.getTranslationUrlArg()+"' "+e);
        }
        List<FieldExtractor> copyFieldNames = copyFieldsExtractor.getFields();
        for (FieldExtractor copyFieldExtractor : copyFieldNames) {
            String fieldN = copyFieldExtractor.getName();
            Boolean fieldL = Boolean.valueOf(copyFieldExtractor.isList());
            fields.put(fieldN, fieldL);
            if(fieldN.endsWith("_html")) {metadata.addFieldAsTextStyling(fieldN, SyntaxKind.Html);}
            else if(fieldN.endsWith("_image")) {metadata.addFieldAsImage(fieldN);}
        }

        // Create Java model context
        IContext context;
        try {
            context = report.createContext();
        } catch (XDocReportException e) {
            throw new RenderException("XDoc Report Exception during report create context: '"+args.getTranslationUrlArg()+"' "+e);
        }

        // loop fields
        for (String fieldName : fields.keySet())
        {
            if(fieldName.endsWith("_image")) {

                try {

                    String docExternalId = "";
                    String attributeValue = "";
                    int attributeTypeIndex = fieldName.indexOf(":");
                    if(attributeTypeIndex>=0) {
                        attributeValue = fieldName.substring(attributeTypeIndex+1, fieldName.lastIndexOf("_image"));
                        String attributeName = fieldName.substring(0,attributeTypeIndex);
                        docExternalId = data.xpathGet("document["+attributeName+"='"+attributeValue+"'][1]/externalSystemId", "").toString();
                    }
                    else {
                        attributeValue = fieldName.substring(0, fieldName.lastIndexOf("_image"));
                        docExternalId = data.xpathGet("document[fileName='"+attributeValue+"'][1]/externalSystemId", "").toString();
                    }

                    if(!docExternalId.isEmpty()) {
                        String protocol = CoreContext.getCoreProxy().getParameterValue("ProductRepository.Protocol");
                        String host = CoreContext.getCoreProxy().getParameterValue("ProductRepository.Host");
                        String port = CoreContext.getCoreProxy().getParameterValue("ProductRepository.Port");
                        URL imageUrl = new URL(protocol+"://"+host+":"+port+"/pageflow-portlet/fetchDocument?reference="+docExternalId);

                        IImageProvider image = new FileImageProvider(new File(imageUrl.toURI()));
                        image.setUseImageSize(true);
                        image.setResize(true);
                        context.put( fieldName, image );
                    }

                } catch (Exception e) {
                    throw new PreconditionException("Image insertion failue: '"+fieldName+"': "+e.toString());
                }
            }
            else {

                try {
                    String fieldValue = "";
                    if(fieldName.endsWith("_html")) {
                        String attributeName = fieldName.substring(0, fieldName.lastIndexOf("_html"));
                        fieldValue = data.xpathGet(attributeName).toString();
                        fieldValue = fieldValue.replaceAll("</p><p>","</p><br/><p>");
                    }
                    else {
                        fieldValue = data.xpathGet(fieldName, fieldName).toString();
                        fieldValue = i18n(fieldValue);
                    }
                    context.put(fieldName, fieldValue);
                } catch (Exception e) {
                    throw new PreconditionException("xpath expression: '"+xpath+"' could not be evaulated: "+e.toString());
                }

            }
        }

        // Convert DOCX 2 PDF
        Options options = null;
        if(pdfaConformance) {
/*                PdfOptions pdfOptions = PdfOptions.getDefault();
                pdfOptions.setConfiguration( new IPdfWriterConfiguration()
                {
                  @Override
                public void configure( PdfWriter writer )
                  {
                    writer.setPDFXConformance( PdfWriter.PDFA1A );
                  }
                });
                options = Options.getTo(ConverterTypeTo.PDF).via(ConverterTypeVia.XWPF).subOptions(pdfOptions);
*/            }
        else {
            options = Options.getTo(ConverterTypeTo.PDF);
        }

        // Generate by merging Java model with the DOCX and output to PDF
        try {
            report.convert(context, options, output);
        } catch (XDocConverterException e) {
            throw new RenderException("XDoc Converter Exception during report pdf conversion: '"+args.getTranslationUrlArg()+"' "+e);
        } catch (XDocReportException e) {
            throw new RenderException("XDoc Report Exception during report pdf conversion: '"+args.getTranslationUrlArg()+"' "+e);
        } catch (IOException e) {
            throw new RenderException("IO Exception during report pdf conversion: '"+args.getTranslationUrlArg()+"' "+e);
        }


        args.setRenderedDocumentRet(output.toByteArray());

        if (args.getRenderedDocumentRet()==null || args.getRenderedDocumentRet().length==0) {
            throw new PostconditionException("args.getRenderedDocumentRet()==null || args.getRenderedDocumentRet().length==0");
        }
    }

    public String i18n(String key) {
        return I18N.i18n(key);
    }

    public String i18n(String key, Object... args) {
        String format = I18N.i18n(key);
        Formatter formatter = new Formatter();
        String ret = formatter.format(format, args).toString();
        formatter.close();
        return ret;
    }

}

