/* Copyright Applied Industrial Logic Limited 2002. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.party;

import static ch.lambdaj.Lambda.filter;
import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static com.ail.core.Functions.isEmpty;
import static javax.persistence.CascadeType.ALL;
import static org.hamcrest.Matchers.is;
import static org.hibernate.annotations.CascadeType.DETACH;
import static org.hibernate.annotations.CascadeType.MERGE;
import static org.hibernate.annotations.CascadeType.PERSIST;
import static org.hibernate.annotations.CascadeType.REFRESH;
import static org.hibernate.annotations.CascadeType.SAVE_UPDATE;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.envers.Audited;

import com.ail.annotation.TypeDefinition;
import com.ail.core.HasDocuments;
import com.ail.core.HasLabels;
import com.ail.core.HasNotes;
import com.ail.core.Identified;
import com.ail.core.Note;
import com.ail.core.Type;
import com.ail.core.document.Document;
import com.ail.core.document.DocumentType;
import com.ail.financial.PaymentMethod;

@TypeDefinition
@Audited
@Entity
@DiscriminatorColumn(name = "parDSC", discriminatorType = DiscriminatorType.STRING)
@NamedQueries({ @NamedQuery(name = "get.party.by.systemId", query = "select par from Party par where par.systemId = ?"),
        @NamedQuery(name = "get.party.by.externalSystemId", query = "select par from Party par where par.externalSystemId = ?"),
        @NamedQuery(name = "get.parties.by.legalName", query = "select par from Party par where par.legalName like ?"),
        @NamedQuery(name = "get.parties.by.emailAddress", query = "select par from Party par where par.emailAddress like ?"),
        @NamedQuery(name = "get.parties.by.postcode", query = "select par from Party par where par.address.postcode like ?"),
        @NamedQuery(name = "get.parties.by.mobileNumber", query = "select par from Party par where par.mobilephoneNumber like ?") })
public class Party extends Type implements HasDocuments, HasNotes, HasLabels, Identified, HasPartyRelationships {
    static final long serialVersionUID = -593625795936961828L;
    public static final String PARTY_ROLE_TYPES_LABEL_DISCRIMINATOR = "party_role_types";
    public static final String PARTY_RELATIONSHIP_TYPES_LABEL_DISCRIMINATOR = "relationship_types";

    @Index(name = "partyId")
    private String partyId;

    @Index(name = "legalName")
    private String legalName;

    @Index(name = "emailAddress")
    private String emailAddress;
    private String mobilephoneNumber;
    private String telephoneNumber;

    private String id;

    @OneToMany(cascade = ALL)
    private List<Note> note;

    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "line1", column = @Column(name = "line1")), @AttributeOverride(name = "line2", column = @Column(name = "line2")),
            @AttributeOverride(name = "line3", column = @Column(name = "line3")), @AttributeOverride(name = "line4", column = @Column(name = "line4")),
            @AttributeOverride(name = "line5", column = @Column(name = "line5")), @AttributeOverride(name = "town", column = @Column(name = "town")),
            @AttributeOverride(name = "county", column = @Column(name = "county")), @AttributeOverride(name = "country", column = @Column(name = "country")),
            @AttributeOverride(name = "postcode", column = @Column(name = "postcode")), @AttributeOverride(name = "fullAddress", column = @Column(name = "fullAddress")) })
    private Address address;

    @Enumerated(EnumType.STRING)
    private ContactPreference contactPreference = ContactPreference.UNDEFINED;

    @OneToMany()
    @Cascade({SAVE_UPDATE, DETACH, MERGE, PERSIST, REFRESH})
    private List<PaymentMethod> paymentMethod = new ArrayList<>();

    @ElementCollection
    private Set<String> label;

    @OneToMany(cascade = ALL)
    @LazyCollection(LazyCollectionOption.TRUE)
    private List<Document> document = new ArrayList<>();

    @OneToMany(cascade = ALL)
    @LazyCollection(LazyCollectionOption.TRUE)
    private List<Document> archivedDocument = new ArrayList<>();

    @OneToMany()
    @LazyCollection(LazyCollectionOption.TRUE)
    @Cascade({SAVE_UPDATE, DETACH, MERGE, PERSIST, REFRESH})
    private List<PartyRelationship> partyRelationship;

    public Party() {
    }

    public Party(String partyId, String legalName, String emailAddress, String mobilephoneNumber, String telephoneNumber, Address address) {
        super();
        this.partyId = partyId;
        this.legalName = legalName;
        this.emailAddress = emailAddress;
        this.mobilephoneNumber = mobilephoneNumber;
        this.telephoneNumber = telephoneNumber;
        this.address = address;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getMobilephoneNumber() {
        return mobilephoneNumber;
    }

    public void setMobilephoneNumber(String mobilephoneNumber) {
        this.mobilephoneNumber = mobilephoneNumber;
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public ContactPreference getContactPreference() {
        return contactPreference;
    }

    public void setContactPreference(ContactPreference contactPreference) {
        this.contactPreference = contactPreference;
    }

    /**
     * Get the party contact preference as a string (as opposed to an instance of
     * ContactPreference).
     *
     * @return String representation of the party contact preference, or null if the contactPreference
     *         property has not been set.
     */
    public String getContactPreferenceAsString() {
        if (contactPreference != null) {
            return contactPreference.name();
        }
        return null;
    }

    public List<PaymentMethod> getPaymentMethod() {
        if (paymentMethod == null) {
            paymentMethod = new ArrayList<>();
        }
        return paymentMethod;
    }

    public void setPaymentMethod(List<PaymentMethod> paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @Override
    public Set<String> getLabel() {
        if (label == null) {
            label = new HashSet<>();
        }

        return label;
    }

    @Override
    public void setLabel(Set<String> labels) {
        this.label = labels;
    }


    @Override
    public List<Note> getNote() {
        if (note == null) {
            note = new ArrayList<>();
        }
        return note;
    }

    @Override
    public void setNote(List<Note> note) {
        this.note = note;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
       this.id = id;
    }

    @Override
    public List<Document> getDocument() {
        if (document == null) {
            document = new ArrayList<>();
        }

        return document;
    }

    @Override
    public void setDocument(List<Document> document) {
        this.document = document;
    }

    @Override
    public List<Document> getArchivedDocument() {
        if (archivedDocument == null) {
            archivedDocument = new ArrayList<>();
        }

        return archivedDocument;
    }

    @Override
    public void setArchivedDocument(List<Document> archivedDocument) {
        this.archivedDocument = archivedDocument;
    }

    @Override
    public void archiveDocument(Document document) {
        if (this.document.contains(document)) {
            this.archivedDocument.add(document);
            this.document.remove(document);
        }
    }

    @Override
    public void restoreDocument(Document document) {
        if (this.archivedDocument.contains(document)) {
            this.document.add(document);
            this.archivedDocument.remove(document);
        }
    }

    @Override
    public Document retrieveDocumentOfType(DocumentType type) {
        return retrieveDocumentOfType(type.longName());
    }

    @Override
    public Document retrieveDocumentOfType(String type) {
        List<Document> res = filter(having(on(Document.class).getType(), is(type)), getDocument());

        if (res.size() == 1) {
            return res.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<PartyRelationship> getPartyRelationship() {
        if (partyRelationship == null) {
            partyRelationship = new ArrayList<>();
        }
        return partyRelationship;
    }

    @Override
    public void setPartyRelationship(List<PartyRelationship> partyRelationship) {
        this.partyRelationship = partyRelationship;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        result = prime * result + ((isEmpty(legalName)) ? 0 : legalName.hashCode());
        result = prime * result + ((document == null) ? 0 : document.hashCode());
        result = prime * result + ((isEmpty(emailAddress)) ? 0 : emailAddress.hashCode());
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        result = prime * result + ((isEmpty(mobilephoneNumber)) ? 0 : mobilephoneNumber.hashCode());
        result = prime * result + ((isEmpty(telephoneNumber)) ? 0 : telephoneNumber.hashCode());
        result = prime * result + ((partyId == null) ? 0 : partyId.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((contactPreference == null) ? 0 : contactPreference.hashCode());
        result = prime * result + ((paymentMethod == null) ? 0 : paymentMethod.hashCode());
        result = prime * result + ((partyRelationship == null) ? 0 : partyRelationship.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Party other = (Party) obj;
        if (address == null) {
            if (other.address != null)
                return false;
        } else if (!address.equals(other.address))
            return false;
        if (legalName == null) {
            if (other.legalName != null)
                return false;
        } else if (!legalName.equals(other.legalName))
            return false;
        if (emailAddress == null) {
            if (other.emailAddress != null)
                return false;
        } else if (!emailAddress.equals(other.emailAddress))
            return false;
        if (label == null) {
            if (other.label != null)
                return false;
        } else if (!label.equals(other.label))
            return false;
        if (mobilephoneNumber == null) {
            if (other.mobilephoneNumber != null)
                return false;
        } else if (!mobilephoneNumber.equals(other.mobilephoneNumber))
            return false;
        if (telephoneNumber == null) {
            if (other.telephoneNumber != null)
                return false;
        } else if (!telephoneNumber.equals(other.telephoneNumber))
            return false;
        if (partyId == null) {
            if (other.partyId != null)
                return false;
        } else if (!partyId.equals(other.partyId))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (contactPreference != other.contactPreference)
            return false;
        if (paymentMethod == null) {
            if (other.paymentMethod != null)
                return false;
        } else if (!paymentMethod.equals(other.paymentMethod))
            return false;
        if (partyRelationship == null) {
            if (other.partyRelationship != null)
                return false;
        } else if (!partyRelationship.equals(other.partyRelationship))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Party [getSystemId()=" + getSystemId() + ", getExternalSystemId()=" + getExternalSystemId() + ", getCreatedDate()=" + getCreatedDate() + ", partyId=" + partyId + ", legalName="
                + legalName + ", emailAddress=" + emailAddress + ", mobilephoneNumber=" + mobilephoneNumber + ", telephoneNumber=" + telephoneNumber + ", address=" + address
                + ", contactPreference=" + contactPreference + "]";
    }
}
