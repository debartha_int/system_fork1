/* Copyright Applied Industrial Logic Limited 2006. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.insurance.policy;

import javax.persistence.Entity;

import org.hibernate.envers.Audited;

import com.ail.annotation.TypeDefinition;
import com.ail.party.Organisation;

/**
 * Type represents a broker who owns products in the system. It acts as a repository for
 * all the contact information (etc) which is needed to identify the organisation, and
 * defines values for various fields on the UI.
 */
@TypeDefinition
@Audited
@Entity
public class Broker extends Organisation {
	private static final long serialVersionUID = -4521508279619758949L;

	/** Trading name if different from legal name. Leave as null if the two are the same */
    private String tradingName;

    /** Direct debit ID number if direct debit payments are to be received */
    private String directDebitIdentificationNumber;

    /** Queries about quotes should be directed here. If null, telephoneNumber is used. */
    private String quoteTelephoneNumber;

    /** Queries about payments should be directed here. If null, telephoneNumber is used. */
    private String paymentTelephoneNumber;

    /** The system sends quotation emails to this address (for system use, not for general emails). */
    private String quoteEmailAddress;

    /** The system sends policy emails to this address (for system use, not for general emails). */
    private String policyEmailAddress;

    /** The name of an individual in the organisation */
    private String contactName;

    /** Number to call in the event of a claim If null, telephoneNumber is used. */
    private String claimTelephoneNumber;

	public Broker() {
	}

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getDirectDebitIdentificationNumber() {
        return directDebitIdentificationNumber;
    }

    public void setDirectDebitIdentificationNumber(String directDebitIdentificationNumber) {
        this.directDebitIdentificationNumber = directDebitIdentificationNumber;
    }

    public String getPaymentTelephoneNumber() {
        return paymentTelephoneNumber!=null ? paymentTelephoneNumber : getTelephoneNumber();
    }

    public void setPaymentTelephoneNumber(String paymentTelephoneNumber) {
        this.paymentTelephoneNumber = paymentTelephoneNumber;
    }

    public String getQuoteEmailAddress() {
        return quoteEmailAddress;
    }

    public void setQuoteEmailAddress(String quoteEmailAddress) {
        this.quoteEmailAddress = quoteEmailAddress;
    }

    public String getPolicyEmailAddress() {
        return policyEmailAddress;
    }

    public void setPolicyEmailAddress(String policyEmailAddress) {
        this.policyEmailAddress = policyEmailAddress;
    }

    public String getQuoteTelephoneNumber() {
        return quoteTelephoneNumber !=null ? quoteTelephoneNumber : getTelephoneNumber();
    }

    public void setQuoteTelephoneNumber(String quoteTelephoneNumber) {
        this.quoteTelephoneNumber = quoteTelephoneNumber;
    }

    public String getTradingName() {
        if (tradingName==null) {
            return getLegalName();
        }

        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public String getClaimTelephoneNumber() {
        return claimTelephoneNumber!=null ? claimTelephoneNumber : getTelephoneNumber();
    }

    public void setClaimTelephoneNumber(String claimTelephoneNumber) {
        this.claimTelephoneNumber = claimTelephoneNumber;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((claimTelephoneNumber == null) ? 0 : claimTelephoneNumber.hashCode());
        result = prime * result + ((contactName == null) ? 0 : contactName.hashCode());
        result = prime * result + ((directDebitIdentificationNumber == null) ? 0 : directDebitIdentificationNumber.hashCode());
        result = prime * result + ((paymentTelephoneNumber == null) ? 0 : paymentTelephoneNumber.hashCode());
        result = prime * result + ((policyEmailAddress == null) ? 0 : policyEmailAddress.hashCode());
        result = prime * result + ((quoteEmailAddress == null) ? 0 : quoteEmailAddress.hashCode());
        result = prime * result + ((quoteTelephoneNumber == null) ? 0 : quoteTelephoneNumber.hashCode());
        result = prime * result + ((tradingName == null) ? 0 : tradingName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Broker other = (Broker) obj;
        if (claimTelephoneNumber == null) {
            if (other.claimTelephoneNumber != null)
                return false;
        } else if (!claimTelephoneNumber.equals(other.claimTelephoneNumber))
            return false;
        if (contactName == null) {
            if (other.contactName != null)
                return false;
        } else if (!contactName.equals(other.contactName))
            return false;
        if (directDebitIdentificationNumber == null) {
            if (other.directDebitIdentificationNumber != null)
                return false;
        } else if (!directDebitIdentificationNumber.equals(other.directDebitIdentificationNumber))
            return false;
        if (paymentTelephoneNumber == null) {
            if (other.paymentTelephoneNumber != null)
                return false;
        } else if (!paymentTelephoneNumber.equals(other.paymentTelephoneNumber))
            return false;
        if (policyEmailAddress == null) {
            if (other.policyEmailAddress != null)
                return false;
        } else if (!policyEmailAddress.equals(other.policyEmailAddress))
            return false;
        if (quoteEmailAddress == null) {
            if (other.quoteEmailAddress != null)
                return false;
        } else if (!quoteEmailAddress.equals(other.quoteEmailAddress))
            return false;
        if (quoteTelephoneNumber == null) {
            if (other.quoteTelephoneNumber != null)
                return false;
        } else if (!quoteTelephoneNumber.equals(other.quoteTelephoneNumber))
            return false;
        if (tradingName == null) {
            if (other.tradingName != null)
                return false;
        } else if (!tradingName.equals(other.tradingName))
            return false;
        return true;
    }
}
