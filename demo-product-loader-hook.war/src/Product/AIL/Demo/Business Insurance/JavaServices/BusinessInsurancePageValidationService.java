
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ail.core.Attribute;
import com.ail.core.Type;

import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.util.Functions;
import com.ail.util.YesNo;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;

public class BusinessInsurancePageValidationService {

    public static void invoke(ExecutePageActionArgument args) throws Exception {

        Type quote = args.getModelArgRet();

        Attribute turnOver = (Attribute) quote.xpathGet("/asset/attribute[id='turnover']", Attribute.class);
        Attribute coverStartDate = (Attribute) quote.xpathGet("/asset/attribute[id='start_date']", Attribute.class);
        Attribute businessStartTrading = (Attribute) quote.xpathGet("/asset/attribute[id='business_start']", Attribute.class);
        Attribute RegistrationNo = (Attribute) quote.xpathGet("/asset/attribute[id='RegistrationNo']", Attribute.class);
        Attribute userName = (Attribute) quote.xpathGet("/asset/attribute[id='UserName']", Attribute.class);
        Attribute firstName = (Attribute) quote.xpathGet("/asset/attribute[id='first_name']", Attribute.class);
        String StartTradingAnswer = String.valueOf(businessStartTrading.getObject());
        String carRegistrationNo = (String) RegistrationNo.getObject();
        String carHolderUserName = (String) userName.getObject();
        String imagePath = "G:\\Morgan.png";
        String address = "https://vision.googleapis.com/v1/images:annotate?key=AIzaSyDNDWI_ztxhV3pyw0iUM-M0oXvmT6u13dA";
        String jsonString = "";
        String[] stringTokes = { "1", "2.", "3.", "4a.", "4b.", "4c.", "7.", "9." };

        if (!carRegistrationNo.trim().equals("") && !carHolderUserName.trim().equals("")) {
            String resultJson = Functions.getDetailsFromRegcheckAPI(carRegistrationNo, carHolderUserName);

            if (resultJson.equalsIgnoreCase("username and registration no is incorrect")) {

                args.setValidationFailedRet(true);
                userName.addAttribute(new Attribute("error.CoverPageValidation", "Incorrect RegistrationNo or UserName", "string"));
                RegistrationNo.addAttribute(new Attribute("error.CoverPageValidation", "Incorrect RegistrationNo or UserName", "string"));

            } else {
                ObjectMapper objectMapper = new ObjectMapper();
                JsonNode jsonNode = objectMapper.readTree(resultJson);
                String ABICode = jsonNode.get("ABICode").asText();
                String description = jsonNode.get("Description").asText();
                String registrationYear = jsonNode.get("RegistrationYear").asText();
                String carMake = jsonNode.path("CarMake").path("CurrentTextValue").asText();
                String carModel = jsonNode.path("CarModel").path("CurrentTextValue").asText();
                String engineSize = jsonNode.path("EngineSize").path("CurrentTextValue").asText();
                String fuelType = jsonNode.path("FuelType").path("CurrentTextValue").asText();
                String makeDescription = jsonNode.get("MakeDescription").asText();
                String modelDescription = jsonNode.get("ModelDescription").asText();
                String immobiliser = jsonNode.path("Immobiliser").path("CurrentTextValue").asText();
                String numberOfSeats = jsonNode.path("NumberOfSeats").path("CurrentTextValue").asText();
                String indicativeValue = jsonNode.path("IndicativeValue").path("CurrentTextValue").asText();
                String driverSide = jsonNode.path("DriverSide").path("CurrentTextValue").asText();
                String transmission = jsonNode.path("Transmission").path("CurrentTextValue").asText();
                String numberOfDoors = jsonNode.path("NumberOfDoors").path("CurrentTextValue").asText();
                String imageUrl = jsonNode.get("ImageUrl").asText();
                String vehicleInsuranceGroup = jsonNode.get("VehicleInsuranceGroup").asText();
                String vehicleIdentificationNumber = jsonNode.get("VehicleIdentificationNumber").asText();
                String engineCode = jsonNode.get("EngineCode").asText();
                String engineNumber = jsonNode.get("EngineNumber").asText();

                quote.xpathSet("/asset/attribute[id='abi_code']/value", ABICode);
                quote.xpathSet("/asset/attribute[id='description']/value", description);
                quote.xpathSet("/asset/attribute[id='registration_year']/value", registrationYear);
                quote.xpathSet("/asset/attribute[id='car_make']/value", carMake);
                quote.xpathSet("/asset/attribute[id='car_model']/value", carModel);
                quote.xpathSet("/asset/attribute[id='engine_size']/value", engineSize);
                quote.xpathSet("/asset/attribute[id='fuel_type']/value", fuelType);
                quote.xpathSet("/asset/attribute[id='make_description']/value", makeDescription);
                quote.xpathSet("/asset/attribute[id='model_description']/value", modelDescription);
                quote.xpathSet("/asset/attribute[id='immobiliser']/value", immobiliser);
                quote.xpathSet("/asset/attribute[id='numberofseats']/value", numberOfSeats);
                quote.xpathSet("/asset/attribute[id='indicativevalue']/value", indicativeValue);
                quote.xpathSet("/asset/attribute[id='driverside']/value", driverSide);
                quote.xpathSet("/asset/attribute[id='transmission']/value", transmission);
                quote.xpathSet("/asset/attribute[id='numberofdoors']/value", numberOfDoors);
                quote.xpathSet("/asset/attribute[id='imageurl']/value", imageUrl);
                quote.xpathSet("/asset/attribute[id='vehicleinsurancegroup']/value", vehicleInsuranceGroup);
                quote.xpathSet("/asset/attribute[id='vehicleidentificationnumber']/value", vehicleIdentificationNumber);
                quote.xpathSet("/asset/attribute[id='enginecode']/value", engineCode);
                quote.xpathSet("/asset/attribute[id='enginenumber']/value", engineNumber);

            }

        }

        if (StartTradingAnswer.equalsIgnoreCase("1.0")) {
            Attribute startDate = (Attribute) quote.xpathGet("/asset/attribute[id='business_start_datefield']", Attribute.class);

            // Business start date validation
            if (!Functions.hasErrorMarkers(startDate)) {

                // get the value of the attribute as a date
                Date answer = (Date) startDate.getObject();

                // Calculate today, test cover date not in past
                Calendar limit = Calendar.getInstance();
                limit.add(Calendar.DATE, -1);
                if (answer.after(limit.getTime())) {
                    args.setValidationFailedRet(true);

                    startDate.addAttribute(new Attribute("error.CoverPageValidation", "Start Date should not be in future date ", "string"));
                }

            }

        }

        // Business turn over validation
        if (!Functions.hasErrorMarkers(turnOver)) {

            // get the value of the attribute as a date
            Long annualTrunOver = (Long) turnOver.getObject();

            if (annualTrunOver <= 200000) {
                args.setValidationFailedRet(true);
                turnOver.addAttribute(new Attribute("error.CoverPageValidation", "We can not generate quote with trunover less than &pound;200,000.00 ", "string"));
            }

        }

        if (!Functions.hasErrorMarkers(coverStartDate)) {

            // get the value of the attribute as a date
            Date answer = (Date) coverStartDate.getObject();

            // Calculate today, test cover date not in past
            Calendar limit = Calendar.getInstance();
            limit.add(Calendar.DATE, 0);
            if (answer.before(limit.getTime())) {
                args.setValidationFailedRet(true);
                coverStartDate.addAttribute(new Attribute("error.CoverPageValidation", "Cover Start Date should not be in past date ", "string"));
            }

        }
        
        // Driving Licence details

        String imageContent = Functions.getImageContent(imagePath);
        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost postRequest = new HttpPost(address);

        String sendDataToBody = "{\r\n" + "  \"requests\":[\r\n" + "    {\r\n" + "      \"image\":{\r\n" + "        \"content\":" + imageContent + "\r\n" + "      },\r\n" + "      \"features\":[\r\n"
                + "        {\r\n" + "          \"type\":\"TEXT_DETECTION\",\r\n" + "          \"maxResults\":10\r\n" + "        }\r\n" + "      ]\r\n" + "    }\r\n" + "  ]\r\n" + "}";

        StringEntity input = new StringEntity(sendDataToBody);

        input.setContentType("application/json");

        postRequest.setEntity(input);

        HttpResponse r = client.execute(postRequest);

        HttpEntity e = r.getEntity();

        jsonString = EntityUtils.toString(e);

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode jsonNode = objectMapper.readTree(jsonString);

        ArrayNode arrayNode = (ArrayNode) jsonNode.get("responses");

        JsonNode finalJsonNode = arrayNode.get(0);

        ArrayNode textAnnotationsNode = (ArrayNode) finalJsonNode.get("textAnnotations");

        JsonNode textAnnotationsJsonNode = textAnnotationsNode.get(0);

        String description = textAnnotationsJsonNode.get("description").asText().replaceAll("\n", "");

        System.out.println("description " + description);

        List<String> result = Functions.getSplitedList(description, stringTokes);

        String lastnameInLicence =(String) result.get(0);

        String firstNameInLicence = (String)result.get(1);
        
        System.out.println("firstNameInLicence "+firstNameInLicence);

        String dob = (String)result.get(2);

        String[] splitDob = dob.split(" ");

        String DobWithOutPlace = splitDob[0];

        String validFrom =(String) result.get(3);

        String validTo = (String)result.get(4);

        String licence = (String)result.get(5);

        String[] splitLicenceNo = licence.split(" ");

        String finalLicenceNo = splitLicenceNo[1] + " " + splitLicenceNo[2];

        String licenceAddress =(String) result.get(6);

        String[] splitAddress = licenceAddress.split("8");

        String finalAddress = splitAddress[1];

        String category = (String)result.get(7);

        quote.xpathSet("/asset/attribute[id='licence_last_name']/value", lastnameInLicence);
        quote.xpathSet("/asset/attribute[id='licence_first_name']/value", firstNameInLicence);
        quote.xpathSet("/asset/attribute[id='date_of_birth']/value", DobWithOutPlace);
        quote.xpathSet("/asset/attribute[id='valid_from']/value", validFrom);
        quote.xpathSet("/asset/attribute[id='valid_to']/value", validTo);
        quote.xpathSet("/asset/attribute[id='licence_number']/value", finalLicenceNo);
        quote.xpathSet("/asset/attribute[id='licence_address']/value", finalAddress);
        quote.xpathSet("/asset/attribute[id='category']/value", category);

    }

}
