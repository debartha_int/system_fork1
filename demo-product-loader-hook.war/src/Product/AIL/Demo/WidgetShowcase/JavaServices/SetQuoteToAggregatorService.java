/* Copyright Applied Industrial Logic Limited 2014. All rights reserved. */
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.core.CoreProxy;
import com.ail.core.language.I18N;
import com.ail.financial.MoneyProvision;
import com.ail.financial.IWinPay;
import com.ail.financial.PaymentSchedule;
import com.ail.insurance.policy.AssessmentSheet;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.Section;
import com.ail.financial.CurrencyAmount;
import com.ail.financial.PaymentMethod;

public class SetQuoteToAggregatorService {
    public static void invoke(ExecutePageActionArgument args) {
        Policy policy = (Policy) args.getModelArgRet();

        policy.setAggregator(true);

        Section section=(Section)new CoreProxy().newProductType("AIL.Demo.WidgetShowcase","AggregatedProductAssessmentSheet");

        policy.setAssessmentSheetList(section.getAssessmentSheetList());
    }
}