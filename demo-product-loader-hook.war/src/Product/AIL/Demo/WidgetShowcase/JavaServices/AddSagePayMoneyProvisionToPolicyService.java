/* Copyright Applied Industrial Logic Limited 2014. All rights reserved. */
import java.util.ArrayList;
import java.util.List;

import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.core.language.I18N;
import com.ail.financial.MoneyProvision;
import com.ail.financial.SagePay;
import com.ail.financial.PaymentSchedule;
import com.ail.insurance.policy.Policy;
import com.ail.financial.CurrencyAmount;
import com.ail.financial.PaymentMethod;

/**
 * The BuyWithSagePayButton requires that the policy contains a SagePay based
 * MoneyProvision at status NEW. This method creates such a MoneyProvision.
 */
public class AddSagePayMoneyProvisionToPolicyService {
    public static void invoke(ExecutePageActionArgument args) {

    	Policy policy = (Policy) args.getModelArgRet();
    	
    	if (policy.getPaymentDetails()==null) {
	    	CurrencyAmount amount = policy.getTotalPremium();
	    	String description = I18N.i18n("i18n_buy_with_sagepay_payment_description");
	    	PaymentMethod pm = new SagePay();
	    	
	    	MoneyProvision mp=new MoneyProvision(amount, pm, description);
	    	
	        List moneyProvision=new ArrayList();
	        
	        moneyProvision.add(mp);
	        
	        PaymentSchedule ps=new PaymentSchedule(moneyProvision, description);
	
	        policy.setPaymentDetails(ps);
    	}
    }
}