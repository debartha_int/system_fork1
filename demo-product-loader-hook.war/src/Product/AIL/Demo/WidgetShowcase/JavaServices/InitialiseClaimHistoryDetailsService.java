import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import com.ail.financial.CurrencyAmount;
import com.ail.insurance.claim.Claim;
import com.ail.insurance.claim.ClaimStatus;
import com.ail.insurance.policy.Policy;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;

/* Copyright Applied Industrial Logic Limited 2007. All rights reserved. */

/**
 * Initialise the necessary fields to enable to ClaimHistory widget to render.
 */
public class InitialiseClaimHistoryDetailsService {
    /**
     * Service entry point.
     */
    public static void invoke(ExecutePageActionArgument args) {
        Policy policy = (Policy) args.getModelArgRet();

        if (policy.getClaim().size() == 0) {
            addClaim(policy, ClaimStatus.OPEN, "12345");
            addClaim(policy, ClaimStatus.REJECTED, "6789");
        }
    }
        
    /**
	 * Add claim with the passed status.
	 * 
	 * @param policy Schedule will be added to args.getOptionsRet()
	 * @param status of claim
	 */ 
    private static void addClaim(Policy policy, ClaimStatus status, String claimNumber) {
        Claim claim = new Claim();
        
        claim.setClaimNumber(claimNumber);
        
        LocalDate endDate = LocalDate.now();
        LocalDate startDate = endDate.minusDays(2);
        
        claim.setStartDate(Date.from(startDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
        claim.setEndDate(Date.from(endDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
        
        claim.setStatus(status);
        
        String outstandingTotalValue = claimNumber.substring(0, claimNumber.length() -2) + "." + claimNumber.substring(claimNumber.length() -2, claimNumber.length());
        
        CurrencyAmount outstandingTotalAmount = new CurrencyAmount(outstandingTotalValue, "GBP");
        claim.setOutstandingTotal(outstandingTotalAmount);
        
        policy.addClaim(claim);
    }
}