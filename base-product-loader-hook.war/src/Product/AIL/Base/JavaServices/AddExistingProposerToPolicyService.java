import org.apache.commons.lang.StringUtils;

import com.ail.core.CoreProxy;
import com.ail.insurance.policy.Policy;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.PageFlowContext;

public class AddExistingProposerToPolicyService {
    public static void invoke(ExecutePageActionArgument args) {

        CoreProxy coreProxy = PageFlowContext.getCoreProxy();
        try {
            Policy policy = (Policy) args.getModelArgRet();

            String policyEntityId = (String)PageFlowContext.getRequestWrapper().getServletRequest().getAttribute("policyEntityId");

            if (StringUtils.isNotBlank(policyEntityId)) {
                Policy existing = (Policy) coreProxy.queryUnique("get.policy.by.systemId", Long.valueOf(policyEntityId));
                if (existing != null) {
                    policy.setClient(existing.getClient());
                }
            }

        } catch (Exception e) {
            coreProxy.logError("Error in AddExistingProposerToPolicyService: " + e);
        }

    }

}
