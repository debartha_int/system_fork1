import java.text.MessageFormat;
import java.util.Date;

import com.ail.insurance.policy.Policy;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.party.Party;

/**
 * Add party id to party
 */
public class AddPartyIdToProposerService {

    public static void invoke(ExecutePageActionArgument args) {

        Policy policy = (Policy) args.getModelArgRet();
        Party proposer = policy.getClient();
        if (proposer != null
                && (proposer.getPartyId() == null || "".equals(proposer.getPartyId().trim()))) {
            long days = new Date().getTime() / 86400000;
            proposer.setPartyId("C" + days + MessageFormat.format("{0,number,000000}", + proposer.getSystemId()));
        }
    }
}
