import com.ail.core.CoreProxy;
import com.ail.core.Functions;
import com.ail.insurance.policy.Policy;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.PageFlowContext;
import com.ail.pageflow.render.NotifyBrokerByEmailService.NotifyBrokerByEmailCommand;

public class SendBrokerEmailNotificationService {
    public static void invoke(ExecutePageActionArgument args) {
        CoreProxy core = PageFlowContext.getCoreProxy();

    	Policy policy=(Policy)args.getModelArgRet();

    	/*
         * use the policy's product type as a namespace so we get the product's
         * configuration.
         */
    	String productName = policy.getProductTypeId();
    	String namespace = Functions.productNameToConfigurationNamespace(productName);

        try {
            NotifyBrokerByEmailCommand command = (NotifyBrokerByEmailCommand) core.newCommand(NotifyBrokerByEmailCommand.class);

            command.setPolicyIdArg(policy.getSystemId());
            command.invoke();
        } catch (Exception e) {
            core.logError("Error in SendBrokerEmailNotificationService:" + e);
        }
    }
}
