/* Copyright Applied Industrial Logic Limited 2017. All rights reserved. */
import java.util.Date;
import java.util.List;

import com.ail.core.CoreProxy;
import com.ail.financial.MoneyProvision;
import com.ail.financial.PaymentSchedule;
import com.ail.pageflow.Action;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.PageFlowContext;

public class QueryPaymentsService {
    /**
     * Query payments
     */
    public static void invoke(ExecutePageActionArgument args) {

        CoreProxy core = PageFlowContext.getCoreProxy();

    	Action action = args.getActionArg();

    	String query = (String)action.xpathGet("attribute[id='queryType']/value", "");
    	Date startDate = (Date)action.xpathGet("attribute[id='startDate']/object", null);
    	Date endDate = (Date)action.xpathGet("attribute[id='endDate']/object", null);

    	@SuppressWarnings("unchecked")
        List<MoneyProvision> payments = (List<MoneyProvision>)core.query("get.pending.payments", startDate, endDate);

    	PaymentSchedule schedule = new PaymentSchedule();
    	schedule.setMoneyProvision(payments);
    	args.setModelArgRet(schedule);
    }
}