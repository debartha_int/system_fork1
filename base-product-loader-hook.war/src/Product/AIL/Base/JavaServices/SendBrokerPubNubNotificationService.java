import com.ail.core.CoreProxy;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.quotation.NotifyBrokerByPubNubService.NotifyBrokerByPubNubCommand;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.PageFlowContext;

public class SendBrokerPubNubNotificationService {
    public static void invoke(ExecutePageActionArgument args) {
        CoreProxy core = PageFlowContext.getCoreProxy();

        Policy policy=(Policy)args.getModelArgRet();

        try {
            NotifyBrokerByPubNubCommand command = (NotifyBrokerByPubNubCommand) core.newCommand(NotifyBrokerByPubNubCommand.class);
            command.setPolicyIdArg(policy.getSystemId());
            command.invoke();
        } catch (Exception e) {
            core.logError("Error in SendBrokerPubNubNotificationService: " + e);
        }
    }
}
