
/* Copyright Inshur Inc. 2016. All rights Reserved */

import static java.net.HttpURLConnection.HTTP_OK;

import java.util.Map;

import com.ail.core.BaseException;
import com.ail.core.CoreContext;
import com.ail.core.HasDocuments;
import com.ail.core.PreconditionException;
import com.ail.core.RestfulServiceInvoker;
import com.ail.core.RestfulServiceReturn;
import com.ail.core.document.Document;
import com.ail.core.language.I18N;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.PageFlowContext;

public class UploadDocumentService extends RestfulServiceInvoker {

	public static void invoke(ExecutePageActionArgument args) throws BaseException {
		new UploadDocumentService().invoke(Argument.class);
	}

	public RestfulServiceReturn service(Argument argument) throws PreconditionException {
		
		if (PageFlowContext.getPolicy() == null) {
			throw new PreconditionException("PageFlowContext.getPolicy() == null");
		}

		Document document = addDocumentToPolicy(argument);

		return new Return(HTTP_OK, document.getExternalSystemId());
	}

	private Document addDocumentToPolicy(Argument arg) {
		Map<String, Object> fileMap = PageFlowContext.getRestfulRequestAttachment();

		Document doc = new Document(arg.documentType, (byte[]) fileMap.get("file"), I18N.i18n(arg.documentType),
				(String) fileMap.get("fileName"), (String) fileMap.get("mimeType"), CoreContext.getProductName());

		if (arg.title != null) {
			doc.setTitle(arg.title);
		}

		doc.setDescription(arg.description);

		if (arg.target != null && arg.target.length() > 0) {
            HasDocuments hasDocuments = (HasDocuments)PageFlowContext.getPolicy().xpathGet(arg.target);
            hasDocuments.getDocument().add(doc);
		} else {
		    PageFlowContext.getPolicy().getDocument().add(doc);
		}

		PageFlowContext.getCoreProxy().flush();

		return doc;
	}

	public static class Argument {
		String documentType;
		String title;
		String description;
		String target;

		public Argument() {
		}

		public Argument(String documentType, String title, String description, String target) {
			this.documentType = documentType;
			this.title = title;
			this.description = description;
			this.target = target;
		}
	}

	public static class Return extends RestfulServiceReturn {
		String documentId;

		public Return(int status, String documentId) {
			super(status);
			this.documentId = documentId;
		}
	}
}