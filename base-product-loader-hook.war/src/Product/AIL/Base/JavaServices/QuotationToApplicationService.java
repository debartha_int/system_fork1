import com.ail.core.CoreProxy;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyStatus;
import com.ail.insurance.quotation.OverrideStatusService.OverrideStatusCommand;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.PageFlowContext;

public class QuotationToApplicationService {
    
    public static void invoke(ExecutePageActionArgument args) throws Exception {
        Policy policy = (Policy) args.getModelArgRet();

        CoreProxy core = PageFlowContext.getCoreProxy();

        OverrideStatusCommand osc = (OverrideStatusCommand) core.newCommand(OverrideStatusCommand.class);
        osc.setPolicyArg(policy);
        osc.setPolicyStatusArg(PolicyStatus.APPLICATION);
        osc.invoke();
        policy = osc.getPolicyArg();
        policy.setQuotationNumber(null);
        policy.setPolicyNumber(null);
        args.setModelArgRet(policy);
       
    }
}
