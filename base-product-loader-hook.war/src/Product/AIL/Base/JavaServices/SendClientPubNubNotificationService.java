import com.ail.core.CoreProxy;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.quotation.NotifyClientByPubNubService.NotifyClientByPubNubCommand;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.PageFlowContext;

public class SendClientPubNubNotificationService {
    public static void invoke(ExecutePageActionArgument args) {
        CoreProxy core = PageFlowContext.getCoreProxy();

        Policy policy=(Policy)args.getModelArgRet();

        try {
            NotifyClientByPubNubCommand command = (NotifyClientByPubNubCommand) core.newCommand(NotifyClientByPubNubCommand.class);
            command.setPolicyIdArg(policy.getSystemId());
            command.invoke();
        } catch (Exception e) {
            core.logError("Error in SendClientPubNubNotificationService: " + e);
        }
    }
}
