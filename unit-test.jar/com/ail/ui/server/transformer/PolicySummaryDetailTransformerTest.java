package com.ail.ui.server.transformer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

import java.text.DateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ail.insurance.policy.Policy;
import com.ail.ui.shared.model.PolicyDetailDTO;

public class PolicySummaryDetailTransformerTest {

    @Mock
    private Policy policy;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testTransform() throws Exception {

        Date createdDate = new Date();
        Date inceptionDate = new Date(createdDate.getTime() + 1);
        Date expiryDate = new Date(createdDate.getTime() + 2);

        doReturn("POL1").when(policy).getPolicyNumber();
        doReturn("QF1").when(policy).getQuotationNumber();
        doReturn("name|address, postcode").when(policy).getId();
        doReturn("ON_RISK").when(policy).getStatusAsString();
        doReturn(createdDate).when(policy).getCreatedDate();
        doReturn(inceptionDate).when(policy).getInceptionDate();
        doReturn(expiryDate).when(policy).getExpiryDate();
        doReturn(3L).when(policy).getMtaIndex();
        doReturn(5L).when(policy).getRenewalIndex();


        PolicyDetailDTO policyDetail = new PolicySummaryDetailTransformer().apply(policy);

        assertEquals("POL1", policyDetail.getPolicyNumber());
        assertEquals("QF1", policyDetail.getQuotationNumber());
        assertEquals("name", policyDetail.getClientName());
        assertEquals("address, postcode", policyDetail.getClientAddress().get(0));
        assertEquals("ONR", policyDetail.getStatus());
        assertEquals("3", policyDetail.getMtaIndex());
        assertEquals("5", policyDetail.getRenewalIndex());
        assertEquals(DateFormat.getDateInstance().format(expiryDate), policyDetail.getExpiryDate());
        assertEquals(DateFormat.getDateInstance().format(inceptionDate), policyDetail.getInceptionDate());
        assertEquals(DateFormat.getDateInstance().format(createdDate), policyDetail.getCreatedDate());

    }

    @Test
    public void testEmptyAddress() throws Exception {

        doReturn("|, ").when(policy).getId();

        PolicyDetailDTO policyDetail = new PolicySummaryDetailTransformer().apply(policy);

        assertEquals("", policyDetail.getClientName());
        assertEquals(0, policyDetail.getClientAddress().size());

        doReturn(null).when(policy).getId();

        policyDetail = new PolicySummaryDetailTransformer().apply(policy);

        assertEquals("", policyDetail.getClientName());
        assertEquals(0, policyDetail.getClientAddress().size());
    }

    @Test
    public void testNullTransform() throws Exception {
        PolicyDetailDTO policyDetail = new PolicySummaryDetailTransformer().apply(policy);
        assertEquals(null, policyDetail.getQuotationNumber());
    }
}
