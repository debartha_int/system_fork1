/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package com.ail.party;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;

public class TestParty {

    private static final String DUMMY_RELATIONSHIP_TYPE = "DUMMY_RELATIONSHIP_TYPE";
    private Party sut;

    @Before
    public void setup() {
        sut = new Party();
    }

    @Test
    public void fetchPartyRelationshipsWithNoParties() {
        Party party1 = mock(Party.class);
        Party party2 = mock(Party.class);
        Party party3 = mock(Party.class);

        sut.addPartyForRelationshipTypes(party1, DUMMY_RELATIONSHIP_TYPE);
        assertThat(sut.fetchPartiesForRelationshipTypes(DUMMY_RELATIONSHIP_TYPE), contains(party1));

        sut.addPartyForRelationshipTypes(party2, DUMMY_RELATIONSHIP_TYPE);
        assertThat(sut.fetchPartiesForRelationshipTypes(DUMMY_RELATIONSHIP_TYPE), contains(party1, party2));

        sut.addPartyForRelationshipTypes(party3, DUMMY_RELATIONSHIP_TYPE);
        assertThat(sut.fetchPartiesForRelationshipTypes(DUMMY_RELATIONSHIP_TYPE), contains(party1, party2, party3));
    }

    @Test
    public void setPartyRelationship() {
        Party party1 = mock(Party.class);
        Party party2 = mock(Party.class);

        sut.setPartyForRelationshipTypes(party1, DUMMY_RELATIONSHIP_TYPE);
        assertThat(sut.fetchPartiesForRelationshipTypes(DUMMY_RELATIONSHIP_TYPE), contains(party1));

        sut.setPartyForRelationshipTypes(party2, DUMMY_RELATIONSHIP_TYPE);
        assertThat(sut.fetchPartiesForRelationshipTypes(DUMMY_RELATIONSHIP_TYPE), contains(party2));
    }

    @Test
    public void setRemotePartyRelationship() {
        Party party1 = mock(Party.class);

        sut.setPartyForRelationshipTypes(party1, DUMMY_RELATIONSHIP_TYPE);
        assertThat(sut.fetchPartiesForRelationshipTypes(DUMMY_RELATIONSHIP_TYPE), contains(party1));

        sut.removePartyFromRelationshipTypes(party1, DUMMY_RELATIONSHIP_TYPE);
        assertThat(sut.fetchPartiesForRelationshipTypes(DUMMY_RELATIONSHIP_TYPE), hasSize(0));
    }

    /**
     * Test valid arguments for the Rate constructor
     *
     * @throws Exception
     */
    @Test
    public void testGoodFormats() throws Exception {
        Address addr = new Address();

        addr.setLine1("The House");
        addr.setLine2("The Road");
        addr.setTown("Town");
        addr.setCounty("County");
        addr.setPostcode("ABC 1DE");

        assertEquals("The House, The Road, Town, County. ABC 1DE", addr.toString());
    }

}
